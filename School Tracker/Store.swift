//
//  Store.swift
//  School Tracker
//
//  Created by Carl Lewis on 5/17/19.
//  Copyright © 2019 Carl Lewis. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

// This class is meant to simulate a central store. Normally, I would have put this in core data.

class Store {
    
    // This is the school data that will trigger the handler of subscribers  when the data loads or reloads
    var schoolData: [[String:String]]{
        didSet {
            for (_, handler)  in Store.subscribers {
                handler()
            }
        }
    }
    
    // SAT data
    var satData: [String: [String: String]] {
        didSet {
            for (_, handler)  in Store.subscribers {
                handler()
            }
        }
    }
    
    static var shared: Store?
    
    static var chosen: [String: String] = [String: String]()
    
    static var subscribers: [String: () -> Void ] = [String: () -> Void]()
    
    // Subscribe function so that I can update subscribers
    static func subscribe(sender: String, handler: @escaping () -> Void) {
        Store.subscribers[sender] = handler
    }
    
    // Provide a way to clean up subscribers
    static func unSubscribe(sender: String) {
        Store.subscribers.removeValue(forKey: sender)
    }
    
    
    // populate school data
    private init(){
        self.schoolData = [[String:String]]()
        self.satData = [String:[String:String]]()
        
        self.getSchoolData()
        self.getSatData()
        
    }
    
    static func initialize() {
        if Store.shared == nil {
            Store.shared = Store()
        }
    }
    
    // Both of these function fetch the school data and the SAT data. I Kept is as as JSON [String: String] Dict for simplicity normally I would create models for the data.
    func getSchoolData(){
        makeRequest(url: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json", handler:  { (data) in
            self.schoolData = data as! [[String : String]]
        })
    }
    
    func getSatData(){
        makeRequest(url: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json", handler: { (data) in
            for i in data {
                if let id = i["dbn"] as Any? {
                    self.satData[id as! String] = i as? [String: String]
                }
            }
        })
    }
    
    
    // abstracted make request. and the handking of data
    func makeRequest (url: String, handler: @escaping ([[String: Any?]]) -> Void ) -> Void {
        var data = [[String: Any?]]()
        AF.request(url).responseJSON { (response) -> Void in
            switch response.result {
            case let .success(value):
                
                let json = JSON(value)
                
                for i in json.arrayValue {
                    var tmp: [String: String] = [:]
                    for ( k, v) in i {
                        tmp[k] = v.stringValue
                    }
                    data.append(tmp)
                }
                handler(data)
            case let .failure(error):
                print(error)
                
            }
        }
    }
}
