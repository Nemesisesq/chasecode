//
//  ViewController.swift
//  School Tracker
//
//  Created by Carl Lewis on 5/15/19.
//  Copyright © 2019 Carl Lewis. All rights reserved.
//

import UIKit

import Alamofire

import SwiftyJSON



class  SchoolTrackerViewController: UITableViewController {
    
    // Row data
    var data: [[String:String]] = [[:]]
    
    //  Load data function handler to reload table data in a scope of my chosing
    func loadData(){
        tableView.reloadData()
    }
    
    // Simulate seting data from a data source
    func setdata(){
        self.data = Store.shared!.schoolData
        tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        Store.subscribe(sender: "self.restorationIdentifier!", handler: setdata)
        self.navigationItem.title = "School Pool"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Store.subscribe(sender: "self.restorationIdentifier!", handler: setdata)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Store.unSubscribe(sender: self.restorationIdentifier!)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolCell", for: indexPath)
        
        let obj = data[indexPath.row]
        
        if let label = cell.viewWithTag(1000) as? UILabel {
            label.text = obj["school_name"]
        }
        return cell
    }
    
    // On press of the row item set the chosen row item in the store.
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       Store.chosen = data[indexPath.row]
    }
}

