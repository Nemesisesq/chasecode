//
//  SchoolDetailViewController.swift
//  School Tracker
//
//  Created by Carl Lewis on 5/16/19.
//  Copyright © 2019 Carl Lewis. All rights reserved.
//

import UIKit

class SchoolDetailViewController: UIViewController {
    
    
    @IBOutlet weak var readingScore: UILabel!
    
    @IBOutlet weak var mathScore: UILabel!
    
    @IBOutlet weak var writingScore: UILabel!
    
    @IBOutlet weak var numberOfTestTakers: UILabel!
    
    var schoolId: String = ""
    
    var name: String = ""
    
    var satData: [String : [String : String]] = [String : [String : String]]()
    
    func goBack() {
        
            let navigationController = self.navigationController!
            navigationController.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        schoolId = Store.chosen["dbn"]!
        
        name = Store.chosen["school_name"]!
        
        self.navigationItem.title = self.name
        
        satData = Store.shared!.satData
        
        guard let data = satData[schoolId] else {
            let alert = UIAlertController(title: "No DBN", message: "For some reason there is no DBN in this data set", preferredStyle: .alert )
            
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: {(action) in self.goBack()}))
            
            self.present(alert, animated: true)
            
            
            return
        }
        
        self.readingScore.text = data["sat_critical_reading_avg_score"]
        
        self.mathScore.text = data["sat_math_avg_score"]
        
        self.writingScore.text = data["sat_writing_avg_score"]
        
        self.numberOfTestTakers.text = data["num_of_sat_test_takers"]
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
